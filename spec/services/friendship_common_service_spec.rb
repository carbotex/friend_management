describe FriendshipCommonService do
  subject { FriendshipCommonService.new(user_email, friend_email).perform }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }
  let(:user_3) { create(:user) }
  let(:user_4) { create(:user) }

  let!(:friendship_1) { create(:friendship, user: user_1, friend: user_3) }
  let!(:friendship_2) { create(:friendship, user: user_1, friend: user_4) }
  let!(:friendship_3) { create(:friendship, user: user_2, friend: user_3) }

  describe '#perform' do
    let(:user_email) { user_1.email }
    let(:friend_email) { user_2.email }

    context 'both users exist' do
      it 'returns sucess' do
        expect(subject).to match_array [user_3.email]
      end
    end

    context "user email doesn't exist" do
      let(:user_email) { 'popo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{user_email} not found.")
      end
    end

    context "friend email doesn't exist" do
      let(:friend_email) { 'jojo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{friend_email} not found.")
      end
    end
  end
end
