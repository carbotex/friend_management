describe FriendshipListService do
  subject { FriendshipListService.new(user_email).perform }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }
  let(:user_3) { create(:user) }

  let!(:friendship_1) { create(:friendship, user: user_1, friend: user_2) }
  let!(:friendship_2) { create(:friendship, user: user_1, friend: user_3) }

  describe '#perform' do
    context "user_1's friends" do
      let(:user_email) { user_1.email }

      it 'returns both user_2 and user_3' do
        expect(subject).to match_array [user_2.email, user_3.email]
      end
    end

    context "user email doesn't exist" do
      let(:user_email) { user_2.email }

      it 'returns both user_2' do
        expect(subject).to be_empty
      end
    end

    context "friend email doesn't exist" do
      let(:user_email) { user_3.email }

      it 'returns both user_3' do
        expect(subject).to be_empty
      end
    end

    context "user email doesn't exist" do
      let(:user_email) { 'popo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{user_email} not found.")
      end
    end
  end
end
