describe SubscriptionService do
  subject { SubscriptionService.new(user_email, target_email).perform }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }

  describe '#perform' do
    let(:user_email) { user_1.email }
    let(:target_email) { user_2.email }

    context 'both emails are valid' do
      it 'returns success' do
        expect { subject }.to change { Subscription.count }.by(1)
      end
    end

    context "user email doesn't exist" do
      let(:user_email) { 'popo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{user_email} not found.")
      end
    end

    context "target email doesn't exist" do
      let(:target_email) { 'target@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{target_email} not found.")
      end
    end

    context 'add existing target' do
      it 'raises exception' do
        expect { subject }.to change { Subscription.count }.by(1)
        expect do
          SubscriptionService.new(user_email, target_email).perform
        end.to raise_error(ErrorHandler::DuplicateRecord, "#{target_email} already exists.")
      end
    end
  end
end
