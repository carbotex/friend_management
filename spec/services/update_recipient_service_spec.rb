describe UpdateRecipientService do
  subject { UpdateRecipientService.new(sender_email, message_body).perform }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }
  let(:user_3) { create(:user) }
  let(:user_4) { create(:user) }

  let(:sender_email) { user_1.email }
  let(:message_body) { 'Hello World!' }

  describe '#perform' do
    context 'no friends, no subscribers, no mentions' do
      it 'returns empty array' do
        expect(subject).to be_empty
      end
    end

    context "sender email doesn't exist" do
      let(:sender_email) { 'popo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{sender_email} not found.")
      end
    end

    context '1 friend, no subscribers, no mentions' do
      let!(:friendship) { create(:friendship, user: user_1, friend: user_3) }
      it "returns friend's email" do
        expect(subject).to match_array(user_3.email)
      end

      context '1 friend, 1 subscriber, no mentions' do
        let!(:subscription) { create(:subscription, user: user_1, subscriber: user_2) }
        it "returns friend's email and subscriber's email" do
          expect(subject).to match_array [user_2.email, user_3.email]
        end

        context '1 friend, 1 subscriber, 1 mention' do
          let(:message_body) { "Hellow World! #{user_4.email}"}

          it "returns friend's, subscriber's and mentioned's email" do
            expect(subject).to match_array [user_2.email, user_3.email, user_4.email]
          end

          context 'blacklisting user_2' do
            let!(:blacklist) { create(:blacklist, user: user_1, block: user_2) }

            it "returns user_3 and user_4 emails" do
              expect(subject).to match_array [user_3.email, user_4.email]
            end
          end

          context 'blacklisting user_3' do
            let!(:blacklist) { create(:blacklist, user: user_1, block: user_3) }

            it "returns user_2 and user_4 emails" do
              expect(subject).to match_array [user_2.email, user_4.email]
            end
          end

          context 'blacklisting user_4' do
            let!(:blacklist) { create(:blacklist, user: user_1, block: user_4) }

            it "returns user_2 and user_3 emails" do
              expect(subject).to match_array [user_2.email, user_3.email]
            end
          end
        end
      end
    end
  end
end
