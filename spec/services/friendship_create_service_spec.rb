describe FriendshipCreateService do
  subject { FriendshipCreateService.new(user_email, friend_email).perform }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }

  describe '#perform' do
    let(:user_email) { user_1.email }
    let(:friend_email) { user_2.email }

    context 'both users exist' do
      it 'returns sucess' do
        expect { subject }.to change { Friendship.count }.by(2)
      end
    end

    context "user email doesn't exist" do
      let(:user_email) { 'popo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{user_email} not found.")
      end
    end

    context "friend email doesn't exist" do
      let(:friend_email) { 'jojo@example.com' }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::UserNotFound, "#{friend_email} not found.")
      end
    end

    context 'friend is in the blacklist' do
      let!(:blacklist) { create(:blacklist, user: user_1, block: user_2) }

      it 'raises exception' do
        expect { subject }.to raise_error(ErrorHandler::FriendshipBlacklistedError, "Requested email address is blacklisted.")
      end
    end
  end
end
