FactoryBot.define do
  factory :subscription do
    association :user, factory: :user, strategy: :build
    association :subscriber, factory: :user, strategy: :build
  end
end
