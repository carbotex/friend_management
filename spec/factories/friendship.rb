FactoryBot.define do
  factory :friendship do
    association :user, factory: :user, strategy: :build
    association :friend, factory: :user, strategy: :build
  end
end
