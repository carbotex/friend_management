FactoryBot.define do
  factory :blacklist do
    association :user, factory: :user, strategy: :build
    association :block, factory: :user, strategy: :build
  end
end
