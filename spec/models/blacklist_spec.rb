describe Blacklist do
  subject { build(:blacklist) }

  describe 'relationships' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:block).class_name(User) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user) }
    it { is_expected.to validate_presence_of(:block) }
    it { is_expected.not_to validate_uniqueness_of(:block).scoped_to(:user) }
  end
end
