describe Subscription do
  subject { build(:subscription) }

  describe 'relationships' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:subscriber).class_name(User) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user) }
    it { is_expected.to validate_presence_of(:subscriber) }
    it { is_expected.not_to validate_uniqueness_of(:subscriber).scoped_to(:user) }
  end
end
