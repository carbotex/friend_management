describe User do
  subject { build(:friendship) }

  describe 'relationships' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:friend).class_name(User) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user) }
    it { is_expected.to validate_presence_of(:friend) }
    it { is_expected.not_to validate_uniqueness_of(:friend).scoped_to(:user) }
  end
end
