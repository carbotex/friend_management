describe User do
  subject { build(:user) }

  describe 'relationships' do
    it { is_expected.to have_many(:friendships) }
    it { is_expected.to have_many(:friends).through(:friendships) }
    it { is_expected.to have_many(:subscriptions) }
    it { is_expected.to have_many(:subscribers).through(:subscriptions) }
    it { is_expected.to have_many(:blacklists) }
    it { is_expected.to have_many(:blocks).through(:blacklists) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_uniqueness_of(:email) }

    context 'email format' do
      subject { build(:user, email: email) }

      context 'when email is not valid' do
        let(:email) { 'invalid@email' }

        it { is_expected.to be_invalid }
      end

      context 'when email is valid' do
        let(:email) { 'invalid@email.cc' }

        it { is_expected.to be_valid }
      end
    end
  end
end
