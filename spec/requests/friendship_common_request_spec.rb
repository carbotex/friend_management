describe FriendshipsController do
  subject { get friendships_common_path, params: params }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }
  let(:user_3) { create(:user) }
  let(:user_4) { create(:user) }
  let(:user_5) { create(:user) }
  let(:user_6) { create(:user) }

  describe '#common' do
    context 'with common friends' do
      let!(:friendship_1) { create(:friendship, user: user_1, friend: user_3) }
      let!(:friendship_2) { create(:friendship, user: user_1, friend: user_4) }
      let!(:friendship_3) { create(:friendship, user: user_1, friend: user_6) }

      let!(:friendship_4) { create(:friendship, user: user_2, friend: user_3) }
      let!(:friendship_5) { create(:friendship, user: user_2, friend: user_4) }
      let!(:friendship_6) { create(:friendship, user: user_2, friend: user_5) }

      let(:params) do
        {
          "friends": [user_1.email, user_2.email]
        }
      end

      let(:expected_result) do
        {
          "success": true,
          "friends": [user_3.email, user_4.email],
          "count": 2
        }.to_json
      end

      it 'return success true and list common friends' do
        subject
        expect(response).to be_successful
        expect(response.body).to eq expected_result
      end
    end

    context 'with no common friends' do
      let!(:friendship_1) { create(:friendship, user: user_1, friend: user_3) }
      let!(:friendship_2) { create(:friendship, user: user_1, friend: user_4) }

      let!(:friendship_4) { create(:friendship, user: user_2, friend: user_5) }
      let!(:friendship_5) { create(:friendship, user: user_2, friend: user_6) }

      let(:params) do
        {
          "friends": [user_1.email, user_2.email]
        }
      end

      let(:expected_result) do
        {
          "success": true,
          "friends": [],
          "count": 0
        }.to_json
      end

      it 'return success true and list common friends' do
        subject
        expect(response).to be_successful
        expect(response.body).to eq expected_result
      end
    end
  end
end
