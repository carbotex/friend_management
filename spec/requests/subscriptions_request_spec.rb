describe SubscriptionsController do
  subject { post subscriptions_path, params: params }

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }

  describe '#create' do
    let(:params) do
      {
        "requestor": user_1.email,
        "target": user_2.email
      }
    end

    let(:expected_result) do
      {
        "success": true
      }.to_json
    end

    it 'return success true and create friendship' do
      expect { subject }.to change { Subscription.count }.by(1)
      expect(response).to be_successful
      expect(response.body).to eq expected_result
    end

    context 'when attempting to add the same record twice' do
      let(:expected_result) do
        {
          "success": false,
          "message": "#{user_2.email} already exists."
        }.to_json
      end

      it 'returns error ' do
        expect { subject }.to change { Subscription.count }.by(1)
        expect { post subscriptions_path, params: params }.to change { Blacklist.count }.by(0)
        expect(response.code).to eq '422'
        expect(response.body).to eq expected_result
      end
    end
  end
end
