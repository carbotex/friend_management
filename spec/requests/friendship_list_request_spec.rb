describe FriendshipsController do
  subject { get friendships_list_path, params: params }
  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }
  let(:user_3) { create(:user) }
  let!(:friendship_1) { create(:friendship, user: user_1, friend: user_2) }
  let!(:friendship_2) { create(:friendship, user: user_1, friend: user_3) }

  describe '#list' do
    let(:params) do
      {
        "email": user_1.email
      }
    end

    let(:expected_result) do
      {
        "success": true,
        "friends": [user_2.email, user_3.email],
        "count": 2
      }.to_json
    end

    it 'return success true and create friendship' do
      subject
      expect(response).to be_successful
      expect(response.body).to eq expected_result
    end
  end
end
