describe UpdatesController do
  subject { get updates_path, params: params }

  let(:params) do
    {
      "sender": sender_email,
      "text": message_body
    }
  end

  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }
  let(:user_3) { create(:user) }

  let!(:friendship) { create(:friendship, user: user_1, friend: user_3) }

  describe '#index' do
    let(:sender_email) { user_1.email }
    let(:message_body) { "Hello World! #{user_2.email}" }

    context 'No Blacklist' do
      let(:expected_result) do
        {
          "success": true,
          "recipients": [user_3.email, user_2.email]
        }.to_json
      end

      it 'returns list of emails which will receive updates' do
        subject
        expect(response).to be_successful
        expect(response.body).to eq expected_result
      end
    end

    context 'With Blacklist' do
      let!(:blacklist) { create(:blacklist, user: user_1, block: user_2) }
      let(:expected_result) do
        {
          "success": true,
          "recipients": [user_3.email]
        }.to_json
      end

      it 'returns list of emails which will receive updates' do
        subject
        expect(response).to be_successful
        expect(response.body).to eq expected_result
      end
    end
  end
end
