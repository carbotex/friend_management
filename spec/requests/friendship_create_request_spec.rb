describe FriendshipsController do
  subject { post friendships_create_path, params: params }
  let(:user_1) { create(:user) }
  let(:user_2) { create(:user) }

  describe '#create' do
    context 'non blacklisted friend' do
      let(:params) do
        {
          "friends": [user_1.email, user_2.email]
        }
      end

      let(:expected_result) do
        {
          "success": true
        }.to_json
      end

      it 'return success true and create friendship' do
        expect { subject }.to change { Friendship.count }.by(2)
        expect(response).to be_successful
        expect(response.body).to eq expected_result
      end

      context 'when attempting to add the same record twice' do
        let(:expected_result) do
          {
            "success": false,
            "message": "#{user_2.email} already exists."
          }.to_json
        end

        it 'returns error ' do
          expect { subject }.to change { Friendship.count }.by(2)
          expect { post friendships_create_path, params: params }.to change { Friendship.count }.by(0)
          expect(response.code).to eq '422'
          expect(response.body).to eq expected_result
        end
      end
    end

    context 'friend is in the blacklist' do
      let(:params) do
        {
          "friends": [user_1.email, user_2.email]
        }
      end

      let(:expected_result) do
        {
          "success": false,
          "message": "Requested email address is blacklisted."
        }.to_json
      end

      let!(:blacklist) { create(:blacklist, user: user_1, block: user_2) }

      it 'return success false and create friendship' do
        expect { subject }.to change { Friendship.count }.by(0)
        expect(response.code).to eq '422'
        expect(response.body).to eq expected_result
      end
    end
  end
end
