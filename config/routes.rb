Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post 'friendships/create', to: 'friendships#create'
  get 'friendships/list', to: 'friendships#list'
  get 'friendships/common', to: 'friendships#common'

  resources :subscriptions, only: :create
  resources :blacklists, only: :create
  resources :updates, only: :index
end
