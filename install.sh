#!/bin/sh
## Script to setup your application
## e.g.
## npm install
## mvn install
## bundle install
## go build

bundle install

bundle exec rake db:drop
bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
