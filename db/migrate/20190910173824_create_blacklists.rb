class CreateBlacklists < ActiveRecord::Migration[5.2]
  def change
    create_table :blacklists do |t|
      t.integer :user_id, index: true
      t.integer :block_id
      t.timestamp
    end
  end
end
