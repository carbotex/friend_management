class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.integer :user_id, index: true
      t.integer :subscriber_id
      t.timestamp
    end
  end
end
