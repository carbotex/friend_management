class BlacklistService
  def initialize(user_email, target_email)
    @user_email = user_email
    @target_email = target_email
  end

  def perform
    user = User.find_by(email: @user_email)
    raise ErrorHandler::UserNotFound.new("#{@user_email} not found.") unless user

    target = User.find_by(email: @target_email)
    raise ErrorHandler::UserNotFound.new("#{@target_email} not found.") unless target

    blacklist = Blacklist.new(user: user, block: target)

    return if blacklist.save

    raise ErrorHandler::DuplicateRecord.new("#{@target_email} already exists.")
  end
end
