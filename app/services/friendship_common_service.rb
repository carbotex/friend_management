class FriendshipCommonService
  def initialize(user_email, friend_email)
    @user_email = user_email
    @friend_email = friend_email
  end

  def perform
    user = User.find_by(email: @user_email)
    raise ErrorHandler::UserNotFound.new("#{@user_email} not found.") unless user

    friend = User.find_by(email: @friend_email)
    raise ErrorHandler::UserNotFound.new("#{@friend_email} not found.") unless friend

    user.friends.map(&:email) & friend.friends.map(&:email)
  end
end
