class UpdateRecipientService
  def initialize(sender_email, message_body)
    @sender_email = sender_email
    @message_body = message_body
  end

  def perform
    @user = User.find_by(email: @sender_email)
    raise ErrorHandler::UserNotFound.new("#{@sender_email} not found.") unless @user

    friends + subscribers + mentions - blacklists
  end

  private

  def friends
    Friendship.where(user: @user).map { |k| k.friend.email }
  end

  def subscribers
    Subscription.where(user: @user).map { |k| k.subscriber.email }
  end

  def mentions
    reg = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i
    return [] if @message_body.blank?
    @message_body.scan(reg).uniq
  end

  def blacklists
    Blacklist.where(user: @user).map { |k| k.block.email }
  end
end
