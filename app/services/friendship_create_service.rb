class FriendshipCreateService
  def initialize(user_email, friend_email)
    @user_email = user_email
    @friend_email = friend_email
  end

  def perform
    user = User.find_by(email: @user_email)
    raise ErrorHandler::UserNotFound.new("#{@user_email} not found.") unless user

    friend = User.find_by(email: @friend_email)
    raise ErrorHandler::UserNotFound.new("#{@friend_email} not found.") unless friend

    if Blacklist.where(user: user, block: friend).any? ||
       Blacklist.where(user: friend, block: friend).any?
      raise ErrorHandler::FriendshipBlacklistedError.new('Requested email address is blacklisted.')
    else
      friendship_1 = Friendship.new(user: user, friend: friend)
      friendship_2 = Friendship.new(user: friend, friend: user)

      unless friendship_1.save
        raise ErrorHandler::DuplicateRecord.new("#{@friend_email} already exists.")
      end

      unless friendship_2.save
        raise ErrorHandler::DuplicateRecord.new("#{@user_email} already exists.")
      end
    end
  end
end
