class FriendshipListService
  def initialize(user_email)
    @user_email = user_email
  end

  def perform
    user = User.find_by(email: @user_email)
    raise ErrorHandler::UserNotFound.new("#{@user_email} not found.") unless user

    user.friends.map(&:email)
  end
end
