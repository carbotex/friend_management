class Blacklist < ApplicationRecord
  belongs_to :user
  belongs_to :block, class_name: 'User'

  validates :user, presence: true
  validates :block, presence: true

  validates :block, uniqueness: {
    scope: :user,
    message: 'only 1 blacklist per 1 person'
  }
end
