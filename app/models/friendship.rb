class Friendship < ApplicationRecord
  belongs_to :user
  belongs_to :friend, class_name: 'User'

  validates :user, presence: true
  validates :friend, presence: true

  validates :friend, uniqueness: {
    scope: :user,
    message: 'only 1 friendship per 1 person'
  }
end
