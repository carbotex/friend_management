class User < ApplicationRecord
  has_many :friendships
  has_many :friends, through: :friendships

  has_many :subscriptions
  has_many :subscribers, through: :subscriptions

  has_many :blacklists
  has_many :blocks, through: :blacklists

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
end
