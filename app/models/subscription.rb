class Subscription < ApplicationRecord
  belongs_to :user
  belongs_to :subscriber, class_name: 'User'

  validates :user, presence: true
  validates :subscriber, presence: true

  validates :subscriber, uniqueness: {
    scope: :user,
    message: 'only 1 subscriber per 1 person'
  }
end
