class ErrorHandler
  class UserNotFound < StandardError; end
  class DuplicateRecord < StandardError; end
  class FriendshipBlacklistedError < StandardError; end
end
