class BlacklistsController < ApplicationController
  def create
    begin
      BlacklistService.new(params[:requestor], params[:target]).perform
      render json: { "success": true }, status: :ok
    rescue ErrorHandler::UserNotFound => e
      render json: { "success": false, "message": e.message }, status: :not_found
    rescue ErrorHandler::DuplicateRecord => e
      render json: { "success": false, "message": e.message }, status: :unprocessable_entity
    end
  end
end
