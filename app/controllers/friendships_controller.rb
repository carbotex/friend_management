class FriendshipsController < ApplicationController
  def create
    begin
      FriendshipCreateService.new(params[:friends].first, params[:friends].last).perform
      render json: { "success": true }, status: :ok
    rescue ErrorHandler::UserNotFound => e
      render json: { "success": false, "message": e.message }, status: :not_found
    rescue ErrorHandler::DuplicateRecord => e
      render json: { "success": false, "message": e.message }, status: :unprocessable_entity
    rescue ErrorHandler::FriendshipBlacklistedError => e
      render json: { "success": false, "message": e.message }, status: :unprocessable_entity
    end
  end

  def list
    begin
      friend_emails = FriendshipListService.new(params[:email]).perform
      render json: { "success": true, "friends": friend_emails, "count": friend_emails.count }, status: :ok
    rescue ErrorHandler::UserNotFound => e
      render json: { "success": false, "message": e.message }, status: :not_found
    end
  end

  def common
    begin
      common_friends = FriendshipCommonService.new(params[:friends].first, params[:friends].last).perform
      render json: { "success": true, "friends": common_friends, "count": common_friends.size }, status: :ok
    rescue ErrorHandler::UserNotFound => e
      render json: { "success": false, "message": e.message }, status: :not_found
    end
  end
end
