class UpdatesController < ApplicationController
  def index
    begin
      recipients = UpdateRecipientService.new(params[:sender], params[:text]).perform
      render json: { "success": true, "recipients": recipients }
    rescue ErrorHandler::UserNotFound => e
      render json: { "success": false, "message": e.message }, status: :not_found
    end
  end
end
