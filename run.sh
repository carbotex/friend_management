#!/bin/sh
## Script to run your application
## e.g.
## npm start
## mvn clean spring-boot:run
## python -m SimpleHTTPServer 8000
## go run main.go
## php -S 0.0.0.0:8000
bundle exec rails server -b 0.0.0.0 -p 8000
