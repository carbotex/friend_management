# Friend Management API

## 1. Create Friendship

* **POST /friendship/create**

  Create a friend connection between two email addresses

* **Body JSON**

  ```
  {
    "friends": ["andy@example.com", "john@example.com"]
  }
  ```
* **Success Response**

  ```
  200 OK

  {
    "success": true
  }
  ```

* **Error Response:**

  When email is already in existing friendship connection.

  ```
  422 UNPROCESSABLE ENTITY

  {
    "success": false,
    "message": "john@example.com already exists."
  }
  ```

  When email is in the blacklist.

  ```
  422 UNPROCESSABLE ENTITY

  {
    "success": false,
    "message": "Requested email address is blacklisted."
  }
  ```

  When email address provided does not exist in database. User registration required.

  ```
  404 NOT FOUND

  {
    "success": false,
    "message": "sule@example.com not found."
  }
  ```

## 2. List Friendship

* **GET /friendship/list**

  Retrieve the friends list for an email address.

* **Body JSON**

  ```
  {
    "email": "andy@example.com"
  }
  ```
* **Success Response**

  ```
  200 OK

  {
    "success": true,
    "friends": [
      "john@example.com"
    ],
    "count": 1
  }
  ```

* **Error Response:**

  When email address provided does not exist in database. User registration required.

  ```
  404 NOT FOUND

  {
    "success": false,
    "message": "sule@example.com not found."
  }
  ```

## 3. List Common Friends

* **GET /friendship/common**

  Retrieve the common friends list between two email addresses.

* **Body JSON**

  ```
  {
    "friends": ["andy@example.com", "john@example.com"]
  }
  ```
* **Success Response**

  ```
  200 OK

  {
    "success": true,
    "friends": [
      "common@example.com"
    ],
    "count": 1
  }
  ```

* **Error Response:**

  When email address provided does not exist in database. User registration required.

  ```
  404 NOT FOUND

  {
    "success": false,
    "message": "sule@example.com not found."
  }
  ```

## 4. Create Subscription.

* **POST /subscriptions**

  Subscribe to updates from an email address.

* **Body JSON**

  ```
  {
    "requestor": "lisa@example.com",
    "target": "john@example.com"
  }
  ```

* **Success Response**

  ```
  200 OK

  {
    "success": true
  }
  ```

* **Error Response:**

  When email address provided does not exist in database. User registration required.

  ```
  404 NOT FOUND

  {
    "success": false,
    "message": "sule@example.com not found."
  }
  ```

  When email address is already subcribed.

  ```
  422 UNPROCESSABLE ENTITY

  {
    "success": false,
    "message": "sule@example.com already exists."
  }
  ```

## 5. Create Blacklist.

* **POST /blacklists**

  Block updates from an email address.

  Suppose "andy@example.com" blocks "john@example.com"
  * if they are connected as friends, then "andy" will no longer receive notifications from "john"
  * if they are not connected as friends, then no new friends connection can be added


* **Body JSON**

  ```
  {
    "requestor": "andy@example.com",
    "target": "john@example.com"
  }
  ```

* **Success Response**

  ```
  200 OK

  {
    "success": true
  }
  ```

* **Error Response:**

  When email address provided does not exist in database. User registration required.

  ```
  404 NOT FOUND

  {
    "success": false,
    "message": "sule@example.com not found."
  }
  ```

  When email address is already blacklisted.

  ```
  422 UNPROCESSABLE ENTITY

  {
    "success": false,
    "message": "sule@example.com already exists."
  }
  ```

## 6. List Updates.

* **GET /updates**

  Retrieve all email addresses that can receive updates from an email address.

  Eligibility for receiving updates from i.e. "john@example.com"
    * has not blocked updates from "john@example.com", and
    * at least one of the following
      * has a friend connection with "john@example.com"
      * has subscribed to updates from "john@example.com"
      * has been @mentioned in the update

* **Body JSON**

  ```
  {
    "sender": "john@example.com",
    "text": "Hello World! kate@example.com"
  }
  ```

* **Success Response**

  ```
  200 OK

  {
    "success": true,
    "recipients": [
      "lisa@example.com",
      "kate@example.com"
    ]
  }
  ```

* **Error Response:**

  When email address provided does not exist in database. User registration required.

  ```
  404 NOT FOUND

  {
    "success": false,
    "message": "sule@example.com not found."
  }
  ```
