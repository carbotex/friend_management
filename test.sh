#!/bin/sh
## Script to test your application
## e.g.
## npm test
## mvn clean test
## python test_unit.py
## go test -v

bundle exec rspec spec
